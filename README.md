# Swagger documents #

## What is this?

This repo is public and contains swagger documentation for projects (JSON format)

## Why?

2 reasons:

- to keep our documentation (and keep track of it)
- to use public access in Swgger UI

## Swagger UI?

[Swagger UI](http://swagger.io/swagger-ui/) is open source standalone website that can render Swagger JSON file in a user friendly format.

Swagger UI can process any JSON file that is publicly available (that is why this repo is public)

## How to use 

Navigate to any Swagger UI instance

- IFD local instance - [10.4.1.226:32770](http://10.4.1.226:32770) - this is only accessible through IFD network
- Official Swagger UI Demo instance - [http://petstore.swagger.io/](http://petstore.swagger.io/)

Find URL of the file that you want

1. Open this repository from browser
2. Click on "Source" icon on the left side menu bar
3. Select your branch from dropdown ("master" by default)
4. Navigate to the file
5. Open file and click on "RAW" button
6. This will show "raw" contents of the file and you can take url that you see in your browser

Copy-paste that url to the Swagger UI address field (top green bar) and click on button "Explore" - this will load JSON file and show you nice pretty Swagger UI

## Neat tricks with Bitbucket URLs

### General format of the RAW file URL

`https://bitbucket.org/{user_repo_owner}/{repo_name}/raw/{commit_number}/{path_to_file}`

for example:

`https://bitbucket.org/lukastosic/swaggertestdoc/raw/68113f2be54323ce5bafc7c143c27fdbcb41aa62/ifdtms/ifdtms.json`

This URL will navigate to this repository, commit number `68113...` and it will navigate to file `/ifdtms/ifdtms.json`

### Use branch/tag name instead of commit number

Of course, having to memorize (or search) by commit numbers is not user friendly, so instead of using commit number, you can replace it with branch name, or if you are using tags, replace it with tag name

For example:

#### Branch name

`https://bitbucket.org/lukastosic/swaggertestdoc/raw/master/ifdtms/ifdtms.json`

This will take latest code from `master` branch

#### Tag name

`https://bitbucket.org/lukastosic/swaggertestdoc/raw/2.0.0/ifdtms/ifdtms.json`

This will take code from tag `2.0.0`
# Contributing to this repository

Make sure to follow guidilens on how to contribute to this repository

## Code merges through pull requests

All code can be merged only through pull requests

## Pull request review

Pull request reviewers must make sure that code is in good shape. All tests are passing and code guidlenies are followed.